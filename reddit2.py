import requests
import json
import pprint
import time
import os
import tkinter
from tkinter.filedialog import askopenfilename, asksaveasfilename
import tkcalendar
import datetime
from math import sqrt
from plot import graph_z

class GUI(tkinter.Frame):
	def __init__(self, master):
		super().__init__(master)
		self.sub_check_value = tkinter.IntVar()
		self.date_check_value = tkinter.IntVar()
		self.date_check = tkinter.Checkbutton(master, text="Within Date Range", variable=self.date_check_value)
		self.subreddit_check = tkinter.Checkbutton(master, text="Inside Subreddit", variable=self.sub_check_value)
		self.score_check_value = tkinter.IntVar()
		self.score_check = tkinter.Checkbutton(master, text="Calculate Relative Popularity (will take time)", variable=self.score_check_value)
		self.keyword_input = tkinter.Entry(master)
		self.subreddit_input = tkinter.Entry(master, state="disabled")
		# These are buttons, when clicked will open a Calendar Object, possibly in popup window
		default_end_date = datetime.date.today()
		default_start_date = default_end_date - datetime.timedelta(weeks=2)
		self.date_before_value = tkinter.StringVar()
		pattern = "%Y-%m-%d"
		self.date_before_value.set(default_end_date.strftime(pattern))
		self.date_after_value = tkinter.StringVar()
		self.date_after_value.set(default_start_date.strftime(pattern))
		self.date_before_input = tkinter.Button(master, textvariable=self.date_before_value, state="disabled")
		self.date_after_input = tkinter.Button(master, textvariable=self.date_after_value, state="disabled")
		####
		self.full_data_location_label = tkinter.Label(master, text="FullData.txt")
		self.text_dump_location_label = tkinter.Label(master, text="TextDump.txt")
		# self.stats_location_label = tkinter.Label(master, text="Stats.txt")
		self.full_data_button = tkinter.Button(master, text="Change")
		self.text_dump_button = tkinter.Button(master, text="Change")
		# self.stats_button = tkinter.Button(master, text="Change")
		self.search_button = tkinter.Button(master, text="Search")

		master.title("Reddit Scraper - Ashland University")

		self.place_gui()
		self.add_functionality()


	def change_file_path(self, label):
		new_location = asksaveasfilename(title="Choose File")
		print(new_location)
		if new_location == () or new_location == "": #User cancelled
			return
		file_name = new_location.split("/")[-1]
		print(file_name)
		label.config(text=file_name)


	def search_click(self):
		term = self.keyword_input.get()
		if term == "":
			return
		subreddit, date_before, date_after = None, None, None
		if self.sub_check_value.get() == 1:
			subreddit = self.subreddit_input.get()
		if self.date_check_value.get() == 1:
			date_before = self.date_before_value.get()
			date_after = self.date_after_value.get()
		self.search(term, self.text_dump_location_label.cget("text"),
					self.full_data_location_label.cget("text"),
					subreddit=subreddit, date_before=date_before, date_after=date_after)

	def search(self, term, dumpLocation, dataLocation, subreddit=None, date_before=None, date_after=None):
		api_arguments = {"size" : 1000, "sort_type" : "score"}
		start_address = "https://api.pushshift.io/reddit/search/submission/?"
		api_arguments["q"] = term
		if not subreddit is None:
			api_arguments["subreddit"] = subreddit
		if not (date_after is None and date_before is None):
			os.environ['TZ'] = 'UTC'
			pattern = "%Y-%m-%d"
			before_epoch = int(time.mktime(time.strptime(date_before, pattern)))
			after_epoch = int(time.mktime(time.strptime(date_after, pattern)))
			api_arguments["after"] = after_epoch
			api_arguments["before"] = before_epoch
		search_request = requests.get(start_address, params=api_arguments)
		results = search_request.json()["data"]
		with open("output", 'w', encoding="utf-8") as out:
			pprint.pprint(results, stream=out)
		print(len(results))
		self.process(results, dumpLocation, dataLocation)


	def compute_popular_ratio(self, search_scores): #Not fully functioning
		# print("Score Input ", search_scores)
		print("Please wait while Popular score Data is collected.")
		# with open("output", 'w') as dump:
		# 	dump.write(str(search_scores))
		api_header = {"User-Agent": "desktop:PsychAnalysis:v0.1 (by /u/Shadow1356)"}
		api_arguments = {"show" : "all", "limit": 100, "restrict_sr" : 1, "sort": "hot"}
		before_arg = None
		results = {}
		scores = []
		while(len(scores) < 1000): #Exit the loop after getting 1000 entries
			api_address = "https://www.reddit.com/r/all/hot.json"
			search_request = requests.get(api_address, params=api_arguments, headers=api_header)
			# print(search_request.url)
			results = search_request.json()
			before_arg = results["data"]["after"]
			api_arguments["after"] = before_arg
			# pprint.pprint(results)
			posts = results["data"]["children"]
			for child in posts:
				scores.append(child["data"]["score"])
			if results["data"]["after"] is None:
				break
		# print(scores)
		# with open("output", 'a') as dump:
		# 	dump.write(str(scores))
		sample_size = len(scores)
		sample_mean_score = sum(scores)/sample_size
		deviation_sum = 0
		for score in scores:
			deviation_sum = deviation_sum + ((score - sample_mean_score)**2)
		sample_standard_deviation = sqrt((1/(sample_size-1))*deviation_sum)
		print("Sample Standard Deviation ", sample_standard_deviation)
		average_searched_score = sum(search_scores)/len(search_scores)
		z_score = (average_searched_score - sample_mean_score) / sample_standard_deviation
		print("Average Score of searched Posts ", average_searched_score)
		print("Mean Score from Popular Sample ", sample_mean_score)
		print("The searched Posts have a z-score of ", z_score)
		graph_z(z_score)
		# print(len(scores))



	def process(self, data, dumpLocation, dataLocation):
		with open(dumpLocation, 'w') as f:
			pass
			processed = []
		search_scores = []
		for child_data in data:
			# pprint.pprint(child_data)
			try:
				# child_data = child["data"]
				trimmed_text = child_data["selftext"].replace("\n", " ")
			except KeyError as ke:
				print("Skipping selftext " + child_data["id"]+ "; missing information")
				trimmed_text = ""
			extracted = {
							"author" : child_data["author"],
							"comment_count" : child_data["num_comments"],
							"score" : child_data["score"],
							"text" : trimmed_text,
							"posted" : child_data["subreddit"],
							"title" : child_data["title"],
							"link" : child_data["full_link"]
						}
			search_scores.append(child_data["score"])
			with open(dumpLocation, 'a', encoding="utf-8") as textFile:
				textFile.write(extracted["author"])
				textFile.write("|")
				textFile.write(str(extracted["comment_count"]))
				textFile.write("|")
				textFile.write(str(extracted["score"]))
				textFile.write("|")
				textFile.write(extracted["title"])
				textFile.write(" ")
				textFile.write(trimmed_text)
				textFile.write("\n")
			processed.append(extracted)

		with open(dataLocation, 'w', encoding="utf-8") as file:
			pprint.pprint(processed, stream=file)
		if(self.score_check_value.get() == 1):
			self.compute_popular_ratio(search_scores)
		print("Processing Search is finished.")

	def add_functionality(self):
		#Export File-locations
		self.full_data_button.configure(command=lambda: self.change_file_path(self.full_data_location_label))
		self.text_dump_button.configure(command=lambda: self.change_file_path(self.text_dump_location_label))
		# self.stats_button.configure(command=lambda: self.change_file_path(self.stats_location_label))
		#Search argument Checkbox stuff
		self.date_check.configure(command=lambda: self.activate_widgets(self.date_check_value, self.date_before_input, self.date_after_input))
		self.subreddit_check.configure(command=lambda: self.activate_widgets(self.sub_check_value, self.subreddit_input))
		#Make date popup 
		self.date_before_input.configure(command=lambda: self.popup_date(self.date_before_value))
		self.date_after_input.configure(command=lambda: self.popup_date(self.date_after_value))
		#Search Button
		self.search_button.configure(command=self.search_click)


	def popup_date(self, value_var):
		def ok_exit():
			nonlocal calendar, value_var, popup
			dateOBJ = calendar.selection_get()
			value_var.set(dateOBJ.strftime("%Y-%m-%d"))
			popup.destroy()

		popup = tkinter.Toplevel()
		calendar = tkcalendar.Calendar(popup, selectbackground="purple")
		calendar.grid(row=0, column=0)
		tkinter.Button(popup, text="OK", command=ok_exit).grid(row=1, column=0)
		tkinter.Button(popup, text="Cancel", command=popup.destroy).grid(row=1, column=1)

	def activate_widgets(self, var, *elements):
		button_state = var.get()
		for widget in elements:
			if button_state == 1:	
				widget.configure(state="normal")
			else:
				widget.configure(state="disabled")

	def place_gui(self):
		tkinter.Label(self.master, text="Keyword Search").grid(row=0, column=0)
		self.keyword_input.grid(row=0, column=1)
		self.subreddit_check.grid(row=1, column=0)
		self.subreddit_input.grid(row=1, column=1)
		self.date_check.grid(row=2, column=0, rowspan=2)
		self.date_after_input.grid(row=2, column=1)
		self.date_before_input.grid(row=3, column=1)
		tkinter.Label(self.master, text="Text For Sentiment Analysis Location").grid(row=4, columnspan=2)
		self.text_dump_location_label.grid(row=5, column=0)
		self.text_dump_button.grid(row=5, column=1)
		tkinter.Label(self.master, text="Complete Data Download Location").grid(row=6, columnspan=2)
		self.full_data_location_label.grid(row=7, column=0)
		self.full_data_button.grid(row=7, column=1)
		# tkinter.Label(self.master, text="Post Statistics Location").grid(row=8, columnspan=2)
		# self.stats_location_label.grid(row=9, column=0)
		# self.stats_button.grid(row=9, column=1)
		self.score_check.grid(row=9, column=0, columnspan=2)
		self.search_button.grid(row=10, columnspan=2)



if __name__ == "__main__":
	# search("Java", subreddit="ProgrammerHumor", date_before="2018-09-26", date_after="2018-09-01")
	# compute_popular_ratio("test", [])
	# search("*")
	# graph_z(4)
	root = tkinter.Tk()
	gui = GUI(root)
	# gui.compute_popular_ratio([9, 2, 5, 4, 12, 7])
	gui.mainloop()
