import requests
import json
import pprint
import time

api_header = {"User-agent": "desktop:PyschAnalysis:v0.1 (by /u/Shadow1356)"}
api_arguments = {"show" : "all", "limit": 100}

def search_recent(search_term, subreddit=None):
	global api_header, api_arguments
	api_arguments["q"] = search_term
	sub_name, api_arguments["restrict_sr"] = calculate_arguments(subreddit)
	api_address = "https://www.reddit.com/r/" + sub_name + "/search.json"
	search_request = requests.get(api_address, params=api_arguments, headers=api_header)
	print(search_request.url)
	results = search_request.json()
	pprint.pprint(results, indent=3)
	# print(len(results))
	process([results])

def search_all(search_term, subreddit=None):
	global api_header, api_arguments
	api_arguments["q"] = search_term
	sub_name, api_arguments["restrict_sr"] = calculate_arguments(subreddit)
	before_arg = None
	results = {}
	to_process = []
	while(True): #don't know when to stop it.
		api_address = "https://www.reddit.com/r/" + sub_name + "/search.json"
		search_request = requests.get(api_address, params=api_arguments, headers=api_header)
		print(search_request.url)
		results = search_request.json()
		before_arg = results["data"]["after"]
		api_arguments["after"] = before_arg
		pprint.pprint(results)
		to_process.append(results)
		if results["data"]["after"] is None:
			break
	process(to_process)

def calculate_arguments(sub):
	name = ""
	restrict = "0"
	if sub is None:
		name = "all"
		restrict = "0"
	else:
		name = sub
		restrict = "1"
	return name, restrict

def process(data):
	with open("textDump", 'w') as f:
		pass
		processed = []
	for page in data:
		result_children = page["data"]["children"]
		print(len(result_children))
		for child in result_children:
			child_data = child["data"]
			trimmed_text = child_data["selftext"].replace("\n", " ")
			extracted = {
							"author" : child_data["author"],
							"comment_count" : child_data["num_comments"],
							"score" : child_data["score"],
							"text" : trimmed_text,
							"posted" : child_data["subreddit"],
							"title" : child_data["title"],
							"link" : child_data["url"]
						}
			with open("textDump", 'a') as textFile:
				textFile.write(extracted["author"])
				textFile.write("|")
				textFile.write(str(extracted["comment_count"]))
				textFile.write("|")
				textFile.write(str(extracted["score"]))
				textFile.write("|")
				textFile.write(extracted["title"])
				textFile.write(" ")
				textFile.write(trimmed_text)
				textFile.write("\n")
			processed.append(extracted)
		with open("fullData", 'w') as file:
			pprint.pprint(processed, stream=file)

if __name__ == "__main__":
	search_recent("Palpatine", "PrequelMemes")