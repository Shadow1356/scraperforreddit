import matplotlib.pyplot as pyplot
from math import floor
import warnings

def graph_z(z_score):
	warnings.filterwarnings("ignore") #it complains because y =[0,0]
	floored = floor(z_score)
	x_min = floored+1
	x_max = floored + 1
	if floored < 0:
		x_max = -(x_max -1)
		x_min -= 1
	else:
		x_min = -x_min
	pyplot.plot(range(x_min, x_max+1), [0 for i in range(x_min, x_max+1)])
	pyplot.axis([x_min, x_max, 0, 0])
	pyplot.plot([z_score], [0], 'ro')
	label = str(z_score) + " Z-score"
	pyplot.annotate(label, xy=(z_score, 0), xytext=(-0.5, 0.00025))
	pyplot.show()


if __name__ == "__main__":
	graph_z(6.43)
	graph_z(-3.5)
	graph_z(0)
	graph_z(0.3554651894)